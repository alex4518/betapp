//
//  StringExtensions.swift
//  BetApp
//
//  Created by Alexandros Albanis on 19/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

extension String {
    
    func removeDecimals() -> String? {
        let delimiter = "."
        let token = self.components(separatedBy: delimiter)
        
        return token.first
    }
}
