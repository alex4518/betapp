//
//  GamesTableViewController.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit

class GamesTableViewController: UITableViewController {

    private var gamesListViewModel: GameListViewModel?
    private var headlinesListViewModel: HeadlinesListViewModel?
    
    private var secondsCounter = 0
    
    private var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateData), userInfo: nil, repeats: true)
        
        self.setupTableView()
        self.getGames()
        self.getHeadlines()
    }
    
    
    //MARK: WebService
    private func getGames() {
        ApiClient.getGames { (games, error) in
            if let games = games {
                
                if let competitions = games.first?.betViews?.first?.competitions { //assuming betviews count is always 1
                    self.gamesListViewModel = GameListViewModel.init(competions: competitions)
                    
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func getHeadlines() {
        ApiClient.getHeadlines { (headlines, error) in
            if let betViews = headlines?.first?.betViews {
                self.headlinesListViewModel = HeadlinesListViewModel.init(betViews: betViews)
                
                self.tableView.reloadData()
            }
        }
    }
    
    private func updateGames() {
        ApiClient.getGames { (games, error) in
             if let games = games {
                 
                 if let competitions = games.first?.betViews?.first?.competitions { //assuming betviews count is always 1
    
                     self.gamesListViewModel = GameListViewModel.init(competions: competitions)
                     self.tableView.reloadData()
                 }
             }
         }
    }
    
    private func updateHealines() {
        ApiClient.updateHeadlines { (headlines, error) in
            if let betViews = headlines?.first?.betViews {
                self.headlinesListViewModel = HeadlinesListViewModel.init(betViews: betViews)
                
                self.tableView.reloadData()
            }
        }
    }
    
    //MARK: TableView configuration
    
    private func setupTableView() {
        self.tableView.register(UINib.init(nibName: "GameTableViewCell", bundle: nil), forCellReuseIdentifier: "GameCell")
        
        self.tableView.register(UINib.init(nibName: "HeadLineTableViewCell", bundle: nil), forCellReuseIdentifier: "HeadlineCell")

        self.tableView.rowHeight = UITableView.automaticDimension
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return (self.gamesListViewModel?.competitions.count ?? 0) + 1 // number of competitions + headlines section
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return self.gamesListViewModel?.competitions[section - 1].events?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.section == 0 { //headlines
           let cell = tableView.dequeueReusableCell(withIdentifier: "HeadlineCell", for: indexPath) as? HeadlineTableViewCell
            cell?.headlinesListViewModel = self.headlinesListViewModel
            
            return cell!
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath) as? GameTableViewCell
        
        if let competitions = self.gamesListViewModel?.competitions {
            let competition = competitions[indexPath.section - 1]
            if let event = competition.events?[indexPath.row] {
                             
                let gameViewModel = GameViewModel.init(event: event)
                
                //update cell only if needed
                if cell?.gameViewModel == nil || cell?.gameViewModel?.competitor1 != cell?.gameViewModel?.competitor1 {
                    cell?.gameViewModel = gameViewModel
                }
            }
        }
        
        return cell!
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section > 0 {
            
            return self.gamesListViewModel?.competionsCaptions?[section - 1]
        }
        
        return ""
    }
}

//MARK: Timer
extension GamesTableViewController {
    @objc private func updateData() {
        //autoscroll to next headline every 5 seconds
        if self.secondsCounter % 5 == 0 {
            if let cell = self.tableView.visibleCells.first as? HeadlineTableViewCell {
                cell.autoScroll()
            }
        }
        //update games & headlines every 2 seconds
        else if self.secondsCounter % 2 == 0 {
            
            self.updateGames()
            self.updateHealines()
        }
        
        //update duration label for all games every second
        for (index, cell) in self.tableView.visibleCells.enumerated() {
            if (index > 0) {
                if let gameCell = cell as? GameTableViewCell {
                    gameCell.increaseDurationLabel()
                }
            }
        }
        
        
        self.secondsCounter += 1
    }
}

