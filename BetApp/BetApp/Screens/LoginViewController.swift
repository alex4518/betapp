//
//  LoginViewController.swift
//  BetApp
//
//  Created by Alexandros Albanis on 17/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var usernameTextfield: BindingTextfield! {
        didSet {
            usernameTextfield.bind {
                self.viewModel.username = $0
            }
        }
    }
    
    @IBOutlet weak var passwordTextfield: BindingTextfield! {
           didSet {
               passwordTextfield.bind {
                   self.viewModel.password = $0
               }
           }
       }
        
    private var viewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.viewModel.login { (token, error) in
            if let _ = error {
                let alert = UIAlertController(title: nil, message: error?.localizedDescription, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                return
            }
            
            self.performSegue(withIdentifier: "GamesSegue", sender: nil)
        }
    }
}

