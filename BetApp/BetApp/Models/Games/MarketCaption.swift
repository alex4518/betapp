//
//  MarketCaption.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct MarketCaption: Codable {
    let betTypeSysname, marketCaption: String?
    let betCaptions: String?
}
