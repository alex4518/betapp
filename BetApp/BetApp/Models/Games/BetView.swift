//
//  BetView.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct BetView: Codable {
    let competitionContextCaption: String?
    let competitions: [Competition]?
    let totalCount: Int?
    let marketCaptions: [MarketCaption]?
    let betViewKey: String?
    let modelType: String?
}
