//
//  LiveData.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct LiveData: Codable {
    let homeGoals, awayGoals, homeCorners, awayCorners: Int?
    let homeYellowCards, awayYellowCards, homeRedCards, awayRedCards: Int?
    let homePenaltyKicks, awayPenaltyKicks: Int?
    let supportsAchievements: Bool?
    let liveStreamingCountries: String?
    let sportradarMatchID: Int?
    let referenceTime: String?
    let referenceTimeUnix: Int?
    let elapsed: String?
    let elapsedSeconds: Double?
    let duration, durationSeconds: Double?
    let timeToNextPhase: String?
    let timeToNextPhaseSeconds: Double?
    let phaseSysname: String?
    let phaseCaption: String?
    let phaseCaptionLong: String?
    let isLive, isInPlay, isInPlayPaused, isInterrupted: Bool
    let supportsActions: Bool
    let timeline: Double?
    let adjustTimeMillis: Int?

    enum CodingKeys: String, CodingKey {
        case homeGoals, awayGoals, homeCorners, awayCorners, homeYellowCards, awayYellowCards, homeRedCards, awayRedCards, homePenaltyKicks, awayPenaltyKicks, supportsAchievements, liveStreamingCountries
        case sportradarMatchID = "sportradarMatchId"
        case referenceTime, referenceTimeUnix, elapsed, elapsedSeconds, duration, durationSeconds, timeToNextPhase, timeToNextPhaseSeconds, phaseSysname, phaseCaption, phaseCaptionLong, isLive, isInPlay, isInPlayPaused, isInterrupted, supportsActions, timeline, adjustTimeMillis
    }
}

