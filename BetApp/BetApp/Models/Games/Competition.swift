//
//  Competition.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct Competition: Codable {
    let betContextID: Int?
    let caption, regionCaption: String?
    let events: [Event]?

    enum CodingKeys: String, CodingKey {
        case betContextID = "betContextId"
        case caption, regionCaption, events
    }
}
