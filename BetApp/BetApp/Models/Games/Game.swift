//
//  Games.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct Game: Codable {
    let betViews: [BetView]?
    let hasHighlights: Bool?
    let totalCount: Int?
    let caption, marketViewType, marketViewKey, modelType: String?
}
