//
//  BetItem.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct BetItem: Codable {
    let id: Int
    let code, caption: String?
    let instanceCaption: String?
    let price: Double?
    let oddsText: String?
    let isAvailable: Bool?
}
