//
//  Market.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct Market: Codable {
    let marketID: Int?
    let betTypeSysname: String?
    let betItems: [BetItem]?

    enum CodingKeys: String, CodingKey {
        case marketID = "marketId"
        case betTypeSysname, betItems
    }
}
