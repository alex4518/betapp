//
//  Event.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct Event: Codable {
    let betContextID: Int?
    let path: String?
    let isHighlighted: Bool?
    let additionalCaptions: AdditionalCaptions?
    let liveData: LiveData?
    let markets: [Market]?
    let hasBetContextInfo: Bool?

    enum CodingKeys: String, CodingKey {
        case betContextID = "betContextId"
        case path, isHighlighted, additionalCaptions, liveData, markets, hasBetContextInfo
    }
}
