//
//  AdditionalCaptions.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct AdditionalCaptions: Codable {
    let type: Int?
    let competitor1: String?
    let competitor1ImageID: Int?
    let competitor2: String?
    let competitor2ImageID: Int?

    enum CodingKeys: String, CodingKey {
        case type, competitor1
        case competitor1ImageID = "competitor1ImageId"
        case competitor2
        case competitor2ImageID = "competitor2ImageId"
    }
}
