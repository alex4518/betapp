//
//  HeadlineBetView.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct HeadlineBetView: Codable {
    let betViewKey, modelType: String?
    let betContextID, marketViewGroupID, marketViewID, rootMarketViewGroupID: Int?
    let path, startTime, competitor1Caption, competitor2Caption: String?
    let marketTags: [String]?
    let betItems: [HeadlineBetItem]?
    let liveData: HeadlineLiveData?
    let displayFormat, text: String?
    let url: String?
    let imageID: Int?

    enum CodingKeys: String, CodingKey {
        case betViewKey, modelType
        case betContextID = "betContextId"
        case marketViewGroupID = "marketViewGroupId"
        case marketViewID = "marketViewId"
        case rootMarketViewGroupID = "rootMarketViewGroupId"
        case path, startTime, competitor1Caption, competitor2Caption, marketTags, betItems, liveData, displayFormat, text, url
        case imageID = "imageId"
    }
}
