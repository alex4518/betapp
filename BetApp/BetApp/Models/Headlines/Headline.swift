//
//  Headline.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct Headline: Codable {
    let betViews: [HeadlineBetView]?
    let caption, marketViewType, marketViewKey, modelType: String?
}
