//
//  HeadlineLiveData.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct HeadlineLiveData: Codable {
    let remaining: String?
    let remainingSeconds: Double?
    let homePoints, awayPoints: Int?
    let quarterScores: [QuarterScore]?
    let homePossession, supportsAchievements: Bool?
    let liveStreamingCountries: String?
    let sportradarMatchID: Int?
    let referenceTime: String?
    let referenceTimeUnix: Int?
    let elapsed: String?
    let elapsedSeconds: Double?
    let duration: String?
    let durationSeconds: Int?
    let timeToNextPhase, timeToNextPhaseSeconds: String?
    let phaseSysname, phaseCaption, phaseCaptionLong: String?
    let isLive, isInPlay, isInPlayPaused, isInterrupted: Bool?
    let supportsActions: Bool?
    let timeline: String?
    let adjustTimeMillis: Int?

    enum CodingKeys: String, CodingKey {
        case remaining, remainingSeconds, homePoints, awayPoints, quarterScores, homePossession, supportsAchievements, liveStreamingCountries
        case sportradarMatchID = "sportradarMatchId"
        case referenceTime, referenceTimeUnix, elapsed, elapsedSeconds, duration, durationSeconds, timeToNextPhase, timeToNextPhaseSeconds, phaseSysname, phaseCaption, phaseCaptionLong, isLive, isInPlay, isInPlayPaused, isInterrupted, supportsActions, timeline, adjustTimeMillis
    }
}
