//
//  HeadlineBetItem.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct HeadlineBetItem: Codable {
    let id: Int?
    let code, caption, instanceCaption: String?
    let price: Double?
    let oddsText: String?
    let isAvailable: Bool?
}
