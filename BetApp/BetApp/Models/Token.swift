//
//  Token.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct Token: Decodable {
    let token_type: String?
    let access_token: String?
}
