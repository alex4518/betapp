//
//  GetHeadlinesRequest.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation
import  SwiftKeychainWrapper

class GetHeadlinesRequest:BaseRequest<[Headline]> {
    
    override init() {
        super.init()
        
        self.httpMethod = .get
        
        self.path = "5d7113ef3300000e00779746"
        
        if let token = KeychainWrapper.standard.string(forKey: "access_token") {

            self.customHeaders = ["Authorization" : "Bearer \(token)"]
        }
    }
}

