//
//  UpdateHeadlinesRequest.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class UpdateHeadlinesRequest: BaseRequest<[Headline]> {

    override init() {
           super.init()
           
           self.httpMethod = .get
           
           self.path = "5d711461330000d135779748"
           
           if let token = KeychainWrapper.standard.string(forKey: "access_token") {

               self.customHeaders = ["Authorization" : "Bearer \(token)"]
           }
       }
}
