//
//  BaseRequest.swift
//  Weather_MVVM
//
//  Created by Alexandros Albanis on 16/1/20.
//  Copyright © 2020 test. All rights reserved.
//

import Foundation

enum HttpMethod: String {
    case post = "POST"
    case get = "GET"
}

class BaseRequest<T> {
    var path = ""
    var httpMethod = HttpMethod.get
    var body: Data? = nil
    var customHeaders: [String: String]?
}
