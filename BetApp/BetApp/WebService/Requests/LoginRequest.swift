//
//  LoginRequest.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit

class LoginRequest: BaseRequest<Token> {

    init(username: String, password: String) {
        super.init()
        
        self.httpMethod = .post
        
        self.path = "5d8e4bd9310000a2612b5448"
        
        do {
            self.body = try JSONSerialization.data(withJSONObject: ["username": username, password: password], options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
