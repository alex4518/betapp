//
//  ApiClient.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

class ApiClient: NSObject {
    

    
    static func login(username: String, password: String, completion: @escaping (Token?, Error?) -> ()) {
        
        let loginRequest = LoginRequest(username: username, password: password)
        
        WebService.shared.perfromRequest(request: loginRequest) { (token, error) in
            guard let token = token else {
                completion(nil, error)
                return
            }
            
            completion(token, nil)
        }
    }
    
    static func getGames(completion: @escaping ([Game]?, Error?) -> ()) {
        
        let gamesRequest = GetGamesRequest()
        
        WebService.shared.perfromRequest(request: gamesRequest) { (response, error) in
             guard let response = response else {
                 completion(nil, error)
                 return
             }
             
             completion(response, nil)
         }
    }
    
    static func updateGames(completion: @escaping ([Game]?, Error?) -> ()) {
        
        let gamesRequest = UpdateGamesRequest()
        
        WebService.shared.perfromRequest(request: gamesRequest) { (response, error) in
             guard let response = response else {
                 completion(nil, error)
                 return
             }
             
             completion(response, nil)
         }
    }
    
    static func getHeadlines(completion: @escaping ([Headline]?, Error?) -> ()) {
        
        let headlinesRequest = GetHeadlinesRequest()
        
        WebService.shared.perfromRequest(request: headlinesRequest) { (response, error) in
             guard let response = response else {
                 completion(nil, error)
                 return
             }
             
             completion(response, nil)
         }
    }
    
    static func updateHeadlines(completion: @escaping ([Headline]?, Error?) -> ()) {
          
          let headlinesRequest = UpdateHeadlinesRequest()
          
          WebService.shared.perfromRequest(request: headlinesRequest) { (response, error) in
               guard let response = response else {
                   completion(nil, error)
                   return
               }
               
               completion(response, nil)
           }
      }
}
