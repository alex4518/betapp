//
//  WebService.swift
//  Weather_MVVM
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 test. All rights reserved.
//

import UIKit

class WebService: NSObject {
    
    static var shared = WebService()
    private let baseUrl = "http://www.mocky.io/v2/"
    
    private override init() {
        super.init()
    }
    
    func perfromRequest<T: Decodable>(request: BaseRequest<T>, completion: @escaping (T?, Error?) -> ()) {
        
        guard let url = URL.init(string: self.baseUrl + request.path) else {return}
        
        //create urlRequest depending on request type
        var urlRequest = URLRequest.init(url: url)
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        //add all custom headers
        if let customHeaders = request.customHeaders {
            for key in customHeaders.keys {
                urlRequest.addValue(customHeaders[key]!, forHTTPHeaderField: key)
            }
        }
        
        urlRequest.httpMethod = request.httpMethod.rawValue

        if request.httpMethod == .post {
            urlRequest.httpBody = request.body
        }
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in

            guard let data = data else {
                DispatchQueue.main.async {
                    completion(nil, error)
                }
                return
            }

            do {
                let model = try JSONDecoder().decode(T.self, from: data)

                DispatchQueue.main.async {
                    completion(model, nil)
                }
            } catch let jsonErr {
                DispatchQueue.main.async {
                    completion(nil, jsonErr)
                }
            }
            
        }.resume()

    }

}
