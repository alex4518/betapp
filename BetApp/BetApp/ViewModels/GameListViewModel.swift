//
//  GameListViewModel.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct GameListViewModel {
    var competitions: [Competition]
    var competionsCaptions: [String]?
    
    init(competions: [Competition]) {
        self.competitions = competions
        
        self.competionsCaptions = competitions.map({ competion in
             return (competion.caption ?? "")
         })
    }
}
