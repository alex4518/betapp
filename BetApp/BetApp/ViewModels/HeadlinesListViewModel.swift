//
//  HeadlinesListViewModel.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct HeadlinesListViewModel {
    var betViews: [HeadlineBetView]?
    
    init(betViews: [HeadlineBetView]) {
        //discard healine if no competitors
        self.betViews = betViews.filter {$0.competitor1Caption != nil && $0.competitor2Caption != nil }
    }
}
