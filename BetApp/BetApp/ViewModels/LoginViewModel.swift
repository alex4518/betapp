//
//  LoginViewModel.swift
//  BetApp
//
//  Created by Alexandros Albanis on 17/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

struct LoginViewModel {
    var username: String?
    var password: String?
    
    var credentialsValid: Bool {
        
        guard username != nil, password != nil else {
            return false
        }
        return username != "" && password != ""
    }
    
    func login(completion: @escaping (Token?, Error?) -> ()) {
        guard let username = self.username, let password = self.password else {
            
            completion(nil, LoginErrors.credentials)
            return
        }
        
        ApiClient.login(username: username, password: password) { (token, error) in

            guard let token = token else {
                completion(nil, error)
                return
            }
            
            
            
            if let accessToken = token.access_token {
                if self.saveToken(token: accessToken) {
                    completion(token, nil)
                }
                else {
                    completion(nil, LoginErrors.token)
                }
            }
            else {
                completion(nil, LoginErrors.token)
            }
        }
    }
    
    func saveToken(token: String) -> Bool {
        //store access token to keychain
        return KeychainWrapper.standard.set(token, forKey: "access_token")

    }
}

