//
//  GameViewModel.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct GameViewModel {
    var competitor1 = ""
    var competitor2 = ""
    var elapsedTime = ""
    
    var event: Event?
    
    init(event: Event) {
        self.event = event
        
        if let competitor1 = event.additionalCaptions?.competitor1 {
            self.competitor1 = competitor1
        }
        
        if let competitor2 = event.additionalCaptions?.competitor2 {
            self.competitor2 = competitor2
        }
        
        if let time = event.liveData?.elapsed {
            
            if let dateComponents = self.componentsFromTimeString(time: time) {
            
                self.elapsedTime = self.formatTime(dateComponents: dateComponents) ?? "00:00:00"
            }
        }
    }
    
    func getEventsForCompetition(competition: Competition) -> [Event]? {
        return competition.events
    }
    
    //breaks duration time string into date components
    func componentsFromTimeString(time: String) -> DateComponents? {
        
        //negative time; bug or maybe game not started
        //handle it by not showing time label
        if time.hasPrefix("-") {
            return nil
        }
        
        let components = time.components(separatedBy: ":")
               
        var hours = 0
        if components.count > 0 {
            let strHours = components.first?.removeDecimals()
            hours = Int(strHours ?? "00") ?? 0
        }
       
        var minutes = 0
        if components.count > 1 {
            let strMinutes = components[1].removeDecimals()
            minutes = Int(strMinutes ?? "00") ?? 0
        }

        var seconds = 0
        if components.count > 2 {
            let strSeconds = components[2].removeDecimals()
            seconds = Int(strSeconds ?? "00") ?? 0
        }
       
       let dateComponents = DateComponents(hour: hours, minute: minutes, second: seconds)
        
        return dateComponents
    }
    
    //format time for display
    func formatTime(dateComponents: DateComponents) -> String? {
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = [.hour, .minute, .second ]
        formatter.zeroFormattingBehavior = [ .pad ]

        let duration = formatter.string(from: dateComponents)
        return duration
    }
    
    //add one second to current duration counter
    mutating func increaseDurationCounter() {
        
        var components = self.componentsFromTimeString(time: self.elapsedTime)
        if components != nil && self.elapsedTime != "" {
        
            let seconds = (components!.second ?? 0) + 1
            
            components!.second = seconds

            self.elapsedTime = self.formatTime(dateComponents: components!) ?? "00:00:00"
        }
    }
}
