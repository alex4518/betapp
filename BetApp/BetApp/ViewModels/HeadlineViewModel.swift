//
//  HeadlineViewModel.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct HeadlineViewModel {
    var competitor1 = ""
    var competitor2 = ""
    var time = ""
    
    init(betView: HeadlineBetView) {
        if let competitor1 = betView.competitor1Caption {
            self.competitor1 = competitor1
        }
        if let competitor2 = betView.competitor2Caption {
            self.competitor2 = competitor2
        }
        
        if let time = betView.startTime {
            self.time = time
            
        }
    }
}
