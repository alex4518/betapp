//
//  HedlineTableViewCell.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit

class HeadlineTableViewCell: UITableViewCell {

 
    @IBOutlet weak var collectionView: UICollectionView!
    
    var headlinesListViewModel: HeadlinesListViewModel? {
        didSet {
            self.collectionView.reloadData()

        }
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        self.collectionView.collectionViewLayout.invalidateLayout()
//    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    self.collectionView.register(UINib.init(nibName: "HeadlineCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HeadlineCollectionCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func autoScroll() {
        
     if let collectionView  = self.collectionView {
            for cell in collectionView.visibleCells {
                let indexPath: IndexPath? = collectionView.indexPath(for: cell)
                if ((indexPath?.row)!  < (self.headlinesListViewModel?.betViews?.count ?? 0) - 1){
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    collectionView.scrollToItem(at: indexPath1!, at: .right, animated: true)
                }
                else{
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                    collectionView.scrollToItem(at: indexPath1!, at: .left, animated: true)
                }
                
            }
        }
        
    }

}



extension HeadlineTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeadlineCollectionCell", for: indexPath) as? HeadlineCollectionViewCell
        
        if let betView = self.headlinesListViewModel?.betViews?[indexPath.row] {
            cell?.viewModel = HeadlineViewModel.init(betView: betView)
        }
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.headlinesListViewModel?.betViews?.count ?? 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
