//
//  HeadlineCollectionViewCell.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit

class HeadlineCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var competitor1Label: UILabel!
    
    @IBOutlet weak var competitor2Label: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    var viewModel: HeadlineViewModel? {
        didSet {
            self.competitor1Label.text = viewModel?.competitor1
            self.competitor2Label.text = viewModel?.competitor2
            self.timeLabel.text = viewModel?.time
        }
    }

}
