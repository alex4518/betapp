//
//  GameTableViewCell.swift
//  BetApp
//
//  Created by Alexandros Albanis on 18/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {

    @IBOutlet weak var competitor1Label: UILabel!
    
    @IBOutlet weak var competitor2Label: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    var gameViewModel: GameViewModel? {
        didSet {
            self.competitor1Label.text = gameViewModel?.competitor1
            self.competitor2Label.text = gameViewModel?.competitor2
            self.timeLabel.text = gameViewModel?.elapsedTime
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func increaseDurationLabel() {
        self.gameViewModel?.increaseDurationCounter()
        self.timeLabel.text = gameViewModel?.elapsedTime
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
