//
//  NSErrorExtensions.swift
//  BetApp
//
//  Created by Alexandros Albanis on 19/1/20.
//  Copyright © 2020 Alexandros Albanis. All rights reserved.
//

import Foundation

struct LoginErrors {
    static var credentials: NSError {
        let userInfo: [AnyHashable : Any] =
            [
                NSLocalizedDescriptionKey :  NSLocalizedString("Please fill in your credentials", comment: "") ,
                NSLocalizedFailureReasonErrorKey : NSLocalizedString("Error", value: "No Credentials", comment: "")
        ]
        
        return NSError(domain: "CredentialsError", code: 200, userInfo: userInfo as? [String : Any])
    }
    
    static var token: NSError {
        let userInfo: [AnyHashable : Any] =
            [
                NSLocalizedDescriptionKey :  NSLocalizedString("Error", value: "Something went wrong", comment: "") ,
                NSLocalizedFailureReasonErrorKey : NSLocalizedString("Error", value: "Could not save token", comment: "")
        ]
        
        return NSError(domain: "TokenError", code: 100, userInfo: userInfo as? [String : Any])
    }
}

